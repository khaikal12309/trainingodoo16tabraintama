from odoo import models, fields, api

from odoo.exceptions import ValidationError

class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    partner_ref = fields.Char('Partner Ref')
    mobile = fields.Char('Mobile', compute='_check_mobile', store=True)
    payment_state = fields.Char('Payment Status')
    new_order_line_ids = fields.One2many('custom_sales_order.order_line', 'order_id', string='Order Line')

    def set_partner_ref(self):
        self.partner_ref = self.partner_id.ref
        self.mobile = self.partner_id.mobile
        self.payment_state = 'Paid'
    
    def action_confirm(self):
        res = super(SaleOrderInherit, self).action_confirm()
        self.partner_ref = 'Test Inherit'
        return res
    
    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id:
            self.partner_ref = self.partner_id.ref
    
    @api.depends('partner_id','partner_id.mobile')
    def _check_mobile(self):
        for rec in self:
            if rec.partner_id:
                rec.mobile = rec.partner_id.mobile

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderInherit, self).default_get(fields)
        partner = self.env['res.partner'].sudo().search([('mobile','=','089123123000')])
        if partner:
            res['partner_id'] = partner.id
        return res
    
    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100