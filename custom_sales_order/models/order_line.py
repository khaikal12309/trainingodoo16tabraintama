from odoo import models, fields, api


class SalesOrderLine(models.Model):
    _name = 'custom_sales_order.order_line'
    _description = 'custom_sales_order.order_line'

    product_template_id = fields.Many2one('product.template', string='Product')
    qty = fields.Float('Quantity')
    order_id = fields.Many2one('sale.order', string='Order')

    